package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface MessageRepository extends JpaRepository <Message, Long> {

    List<Message> findAllByChatId(Long chatId);
    @Query(value = "SELECT * FROM  message  WHERE chat_id = ?1 ORDER BY id DESC LIMIT 10", nativeQuery=true)
    List<Message> getTenMessagesByChatId(Long chatId);

    @Query(value = "SELECT * FROM message  WHERE user_id = ?1 ORDER BY id DESC LIMIT 10", nativeQuery=true)
    List<Message> getTenMessageByUserId(Long userId);
@Query(value = "select * from message where chat_id=1? and user_id=2? order by desc limit 10", nativeQuery = true)
    List<Message> getTenMessageByUserandChatId(Long user_id,Long chat_id);

List<Message> findByChatIdAndUserIdNot(Long chatId,Long userId);

}
