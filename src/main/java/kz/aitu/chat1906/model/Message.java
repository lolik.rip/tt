package kz.aitu.chat1906.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "chat_id")
    private Long chatId;

    private char text;
    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updatedTimestamp")
    private Long updatedTimestamp;

    @Column(name = "delivered_timestamp")
    private Long deliveredTimestamp;

    @Column(name = "is_delivered")
    private boolean delivered;

    @Column(name = "is_read")
    private boolean read;

    @Column(name = "read_timestamp")
    private Long readTimestamp;

    @Column(name="message_type")
    private String messageType;


}
