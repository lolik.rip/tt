alter table message add column message_type varchar(16);
alter table user add column status varchar(16);
alter table message add column is_delivered boolean;
alter table message add column is_read boolean;
alter table user add column read_timestamp bigint;
alter table user add column delivered_timestamp bigint;